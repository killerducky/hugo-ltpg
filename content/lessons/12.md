---
title: "Seki"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "12"
puzzles:
- id: pzl1
  target: 12-1
  text: Puzzle 1
- id: pzl2
  target: 12-2
  text: Puzzle 2
returnTo: "index.html#techniques"
section: "index.html#techniques"
---

# | Seki
## Still alive, but barely

> Having two eyes is not the only way to live! There is another, more dramatic, way.  

**Stones are only dead if they can eventually be captured**

{{< tsumego >}}

{{< rule >}}
	There are {{< black "no points" >}} for groups in seki.  
{{< /rule >}}

All said and done; these players are at an impasse. Both players are better off not attacking the other. Therefore, they agree to truce. In scoring, these groups bring no points whatsoever. It can be disappointing to be reduced to seki but it is a hell of a lot better than dying!

Seki comes in many different shapes and sizes.
{{< extra "1" "More examples of seki" >}}
These are also pretty simple seki examples. Try to imagine why neither player can capture the other.
<br><br><img src="/images/seki.jpg" alt="counting diagram""><br>
<br><img src="/images/seki2.jpg" alt="counting diagram""><br>
{{< /extra >}}